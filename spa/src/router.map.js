import TimeListComponent  from './components/TimeList.vue';
import TimeJogoComponent from './components/TimeJogo.vue';
import TimeZonaComponent from './components/TimeZona.vue';
import LoginComponent from  './components/Login.vue';
import LogoutComponent from  './components/Logout.vue';

export default[
    {
        name: 'login',
        path: '/login',
        component: LoginComponent,
        meta: {auth: false}
    },
    {
        name: 'logout',
        path: '/logout',
        component: LogoutComponent,
        meta: {auth: true}
    },
    {
        name: 'times',
        path: '/times',
        component: TimeListComponent,
        meta: {auth: true}
    },
    {
        name: 'jogo',
        path: '/times/jogo',
        component: TimeJogoComponent,
        meta: {auth: true}
    },
    {
        name: 'zona',
        path: '/times/zona',
        component: TimeZonaComponent,
        meta: {auth: true}
    }

];