/**
 * Created by laercio on 19/01/17.
 */
import Vue from "vue";
import Vuex from "vuex";
import JwtToken from './service/jwt-token';
import {Time} from './time';
import {ResourceTime,ResourceUser} from './service/resource';
import SessionStorage from './service/session-storage';

Vue.use(Vuex);

const state = {
    times: [],
    auth: {
        isAuth: JwtToken.token != null,
        user: SessionStorage.getObject('user')
    }
};

const mutations = {
    'set-times'(state, times){
         state.times = times;
    },
    update(state, time){
        let index = state.times.findIndex(element => time.id == element.id);
        if (index != -1) {
            state.times[index] = time;
        }
    },
    setUser(state, user){
        SessionStorage.setObject('user', user);
        state.auth.user = user;
    },
    authenticated(state){
        state.auth.isAuth = true;
    },
    unauthenticated(state){
        state.auth.isAuth = false;
        state.auth.user = null;
        SessionStorage.remove('user');
        SessionStorage.remove('token');
    }
};
const actions = {
    'load-times'(context) {
        ResourceTime.query().then(response => {
            let times = response.data.map(element => new Time(element.id, element.nome, element.escudo));
            context.commit('set-times', times);
        });

    },
    login(context, {email, password}){
        JwtToken.accessToken(email, password).then((response) => {
            context.commit('authenticated');
            context.dispatch('getUser');
            return response;
        });
    },
    logout(context){
        context.commit('unauthenticated');
    },
    getUser(context){
        ResourceUser.query().then((response) => {
            context.commit('setUser', response.data.user);
        });
    }
};
export default new Vuex.Store({
    state,
    getters: {
        timesLibertadores: state => state.times.slice(0, 6),
        timesRebaixados: state => state.times.slice(16, 20),
    },
    mutations,
    actions
});