/**
 * Created by laercio on 23/01/17.
 */
import  SessionStorage from './session-storage';
import {Jwt} from './resource';

export default{
    get token(){
        return SessionStorage.get('token');
    },
    set token(value){
        return SessionStorage.set('token', value);
    },
    accessToken(email, password) {
        return Jwt.accessToken(email, password).then((response) => {
            this.token = response.data.token;
        });
    },
    getAuthorizationHeader(){
        return `Bearer ${this.token}`;
    },
    refreshToken(){
        return Jwt.refreshToken().then((response) => {
            this.token = response.data.token;
            return response;
        });
    }
}