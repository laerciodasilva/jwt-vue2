/**
 * Created by laercio on 23/01/17.
 */
export default {
    set(key, value){
        window.sessionStorage[key] = value;
        return window.sessionStorage[key];
    },
    get(key, defaultValue){
        return window.sessionStorage[key] || defaultValue;
    },
    remove(key){
        window.sessionStorage.removeItem(key);
    },
    setObject(key, value){
        //Serializar objeto
        window.sessionStorage[key] = JSON.stringify(value);
        return this.getObject(key)
    },
    getObject(key){
        //Deserializar
        return JSON.parse(window.sessionStorage[key] || null);
    }
}